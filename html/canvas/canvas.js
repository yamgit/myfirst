
var canvas=document.getElementById("canvas");
var marcoCanvas=canvas.getContext("2d");
console.log(marcoCanvas);

var lineas=0,maxLineas=30,espacio=10;
var color='#BBC',colorBorde="#afa";

var xIzqSuperior,yIzqSuperior;
var xIzqInferior,yIzqInferior;
var xDerSuperior,yDerSuperior;
var xDerInferior,yDerInferior;

var xEje1,yEje1,xEje2,yEje2;

//Lineas de Bordes
dibujandoLinea(colorBorde,1,1,1,300);
dibujandoLinea(colorBorde,1,299,299,299);
dibujandoLinea(colorBorde,1,1,300,1);
dibujandoLinea(colorBorde,300,1,299,299);
while (lineas<maxLineas)
{
    //Curvatura izq sup
    xIzqSuperior=espacio*(lineas+1);
    yIzqSuperior=300-(espacio*lineas)
    dibujandoLinea(color,xIzqSuperior,0,0,yIzqSuperior);

    //Curvatura izq inferior
    yIzqInferior=espacio*lineas;
    xIzqInferior=espacio*(lineas+1);
    dibujandoLinea(color,0,yIzqInferior,xIzqInferior,300);

    //Curvatura der sup
    xDerSuperior=290-(lineas*espacio);
    yDerSuperior=300-(lineas*espacio);
    dibujandoLinea(color,xDerSuperior,0,300,yDerSuperior);

    //Curvatura der inferior
    xDerInferior=lineas*espacio;
    yDerInferior=290-(lineas*espacio);
    dibujandoLinea(color,300,yDerInferior,xDerInferior,300);

    ++lineas;
}

function dibujandoLinea(color,xInicial,yInicial,xFinal,yFinal)
{
  marcoCanvas.beginPath();
  marcoCanvas.strokeStyle=color;
  marcoCanvas.moveTo(xInicial,yInicial);
  marcoCanvas.lineTo(xFinal,yFinal);
  marcoCanvas.stroke();
  marcoCanvas.closePath();
}


/*
lineas=0;

do
{


  ++lineas;
} while (lineas<maxLineas);




for (lineas= 0; lineas < maxLineas;lineas++)
{


}



for (lineas= 0; lineas < maxLineas;lineas++)
{


}

Trabajando con While
while (lineas<maxLineas)
{
  yIni=espacio*lineas;
  xFin=espacio*(lineas+1);
  dibujandoLinea(color,0,yIni,xFin,300);
  ++lineas;
}

Trabajando con for
for (; lineas < maxLineas; lineas++)
{
  yIni=espacio*lineas;
  xFin=espacio*(lineas+1);
  dibujandoLinea(color,0,yIni,xFin,300);
}


do
{

  yIni=300-(espacio*lineas);
  xFin=espacio*(lineas+1);
  dibujandoLinea(color,0,yIni,xFin,0);
  ++lineas

} while (lineas<maxLineas);


for (lineas= 0;lineas <maxLineas; lineas++)
{
  yIni=espacio*lineas;
  xFin=espacio*(lineas+1);
  dibujandoLinea(color,0,yIni,xFin,300);
}
*/
