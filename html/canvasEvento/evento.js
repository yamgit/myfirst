var canvas=document.getElementById("canvas");
var marco=canvas.getContext("2d");

var lineas=0,espacio=10,maxLineas=30;
var color='#BBC',colorBorde="#afa";
var yIzqInferior,xIzqInferior;

var texto=document.getElementById("numeroLineas");
var boton=document.getElementById("dale");

boton.addEventListener("click",dibujoPorClick);

dibujandoLinea(color,1,1,1,300);
dibujandoLinea(color,1,299,299,299);

while (lineas<maxLineas)
{
  yIzqInferior=espacio*lineas;
  xIzqInferior=espacio*(lineas+1);
  dibujandoLinea(color,0,yIzqInferior,xIzqInferior,300);
  lineas++;
}

function dibujandoLinea(color,xInicial,yInicial,xFinal,yFinal)
{
marco.beginPath();
marco.strokeStyle=color;
marco.moveTo(xInicial,yInicial);
marco.lineTo(xFinal,yFinal);
marco.stroke();
marco.closePath();
}
function dibujoPorClick()
{

}
